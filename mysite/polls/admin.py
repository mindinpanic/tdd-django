# -*- encoding: utf-8 -*-

__author__ = 'ds'

from django.contrib import admin
from polls.models import Poll

admin.site.register(Poll)